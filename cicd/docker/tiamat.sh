#!/bin/bash -i

# Fail on errors.
set -e

# Make sure .bashrc is sourced
. /root/.bashrc

# Allow the workdir to be set using an env var.
# Useful for CI pipiles which use docker for their build steps
# and don't allow that much flexibility to mount volumes
WORKDIR=${SRCDIR:-/src}

cd $WORKDIR

# Install the requirements file if it exists
if [ -f requirements.txt ]; then
    pip install -r requirements.txt
fi

# Install the package to the system python and add it to the path
if [ -f setup.py ]; then
    pip install -e .
fi

# This is how to use the container
echo docker run -v "/path/to/source_code:/src/" salt-tiamat -n target_name


CMD="tiamat build --directory=$WORKDIR"
for ARG in "$@"; do
  CMD+=" ${ARG}"
done

echo "${CMD}"

$CMD
chown -R --reference=. ./dist/
