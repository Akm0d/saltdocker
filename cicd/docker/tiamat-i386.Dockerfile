FROM cdrx/pyinstaller-linux:python3-32bit
SHELL ["/bin/bash", "-i", "-c"]

RUN git clone --branch refactor https://gitlab.com/Akm0d/tiamat.git /tmp/tiamat-src
RUN /root/.pyenv/shims/python3 -m pip install -e /tmp/tiamat-src

COPY tiamat.sh /tiamat.sh
RUN chmod +x /tiamat.sh

ENTRYPOINT ["/tiamat.sh"]
