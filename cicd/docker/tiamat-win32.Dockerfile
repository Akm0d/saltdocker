FROM cdrx/pyinstaller-windows:python3-32bit
SHELL ["/bin/bash", "-i", "-c"]

# Install git
RUN apt-get update && apt-get install -y git

# upgrade pip
RUN /usr/bin/python -m pip install --upgrade pip wheel

# Install tiamat
RUN git clone --branch refactor https://gitlab.com/Akm0d/tiamat.git /tmp/tiamat-src
RUN /usr/bin/pip install -e /tmp/tiamat-src
RUN echo 'wine '\''C:\Python37\Scripts\tiamat.exe'\'' "$@"' > /usr/bin/tiamat
RUN chmod +x /usr/bin/tiamat

COPY tiamat.sh /tiamat.sh

ENTRYPOINT ["/tiamat.sh"]
